package top.hmtools.wxmp.core.model;

import top.hmtools.wxmp.core.configuration.AppIdSecretPair;

/**
 * 用于请求微信服务api获取access token的参数实体类
 * @author HyboWork
 *
 */
public class GetAccessTokenParam {

	private String grant_type = "client_credential";
	
	private String appid;
	
	private String secret;

	public GetAccessTokenParam(AppIdSecretPair appIdSecretPair) {
		if(appIdSecretPair == null ||
				appIdSecretPair.getAppid() == null ||
				appIdSecretPair.getAppsecret() == null
				){
			throw new RuntimeException("入参appid，appsecret数据为空");
		}
		this.appid = appIdSecretPair.getAppid();
		this.secret = appIdSecretPair.getAppsecret();
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getGrant_type() {
		return grant_type;
	}
	
	
}
