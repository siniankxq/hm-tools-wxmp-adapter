package top.hmtools.wxmp.user.apis;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.user.BaseTest;
import top.hmtools.wxmp.user.model.BatchBlackListParam;
import top.hmtools.wxmp.user.model.BlackListParam;
import top.hmtools.wxmp.user.model.BlackListResult;

public class IBlackListApiTest extends BaseTest {
	
	private IBlackListApi blackListApi;
	

	@Test
	public void testGetBlackList() {
		BlackListParam blackListParam = new BlackListParam();
		
		BlackListResult blackList = this.blackListApi.getBlackList(blackListParam);
		this.printFormatedJson("1. 获取公众号的黑名单列表", blackList);
	}

	@Test
	public void testBatchBlackList() {
		BatchBlackListParam batchBlackListParam = new BatchBlackListParam();
		List<String> openid_list = new ArrayList<>();
		openid_list.add("o-OZ0v8HcoPJiNlZLTOZYrkeZUG0");
		batchBlackListParam.setOpenid_list(openid_list);
		ErrcodeBean batchBlackList = this.blackListApi.batchBlackList(batchBlackListParam);
		this.printFormatedJson("2. 拉黑用户", batchBlackList);
	}

	@Test
	public void testBatchUnblackList() {
		BatchBlackListParam batchBlackListParam = new BatchBlackListParam();
		List<String> openid_list = new ArrayList<>();
		openid_list.add("o-OZ0v8HcoPJiNlZLTOZYrkeZUG0");
		batchBlackListParam.setOpenid_list(openid_list);
		ErrcodeBean batchUnblackList = this.blackListApi.batchUnblackList(batchBlackListParam);
		this.printFormatedJson("3. 取消拉黑用户", batchUnblackList);
	}

	@Override
	public void initSub() {
		this.blackListApi = this.wxmpSession.getMapper(IBlackListApi.class);
	}

}
