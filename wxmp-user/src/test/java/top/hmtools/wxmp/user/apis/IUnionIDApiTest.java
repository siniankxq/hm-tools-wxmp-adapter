package top.hmtools.wxmp.user.apis;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import top.hmtools.wxmp.user.BaseTest;
import top.hmtools.wxmp.user.model.BatchUserInfoParam;
import top.hmtools.wxmp.user.model.BatchUserInfoResult;
import top.hmtools.wxmp.user.model.UserInfoParam;
import top.hmtools.wxmp.user.model.UserInfoResult;

public class IUnionIDApiTest extends BaseTest{
	
	private IUnionIDApi iUnionIDApi ;
	

	@Test
	public void testGetUserInfo() {
		UserInfoParam userInfoParam = new UserInfoParam();
		userInfoParam.setOpenid("o-OZ0v8HcoPJiNlZLTOZYrkeZUG0");
		UserInfoResult userInfo = this.iUnionIDApi.getUserInfo(userInfoParam);
		this.printFormatedJson("获取用户基本信息(UnionID机制)-获取用户基本信息（包括UnionID机制）", userInfo);
	}

	@Test
	public void testGetBatchUserInfo() {
		BatchUserInfoParam batchUserInfoParam = new BatchUserInfoParam();
		
		UserInfoParam userInfoParam = new UserInfoParam();
		userInfoParam.setOpenid("o-OZ0v8HcoPJiNlZLTOZYrkeZUG0");
		
		List<UserInfoParam> user_list = new ArrayList<>();
		user_list.add(userInfoParam);
		batchUserInfoParam.setUser_list(user_list);
		BatchUserInfoResult batchUserInfo = this.iUnionIDApi.getBatchUserInfo(batchUserInfoParam);
		this.printFormatedJson("获取用户基本信息(UnionID机制)-批量获取用户基本信息", batchUserInfo);
	}

	@Override
	public void initSub() {
		this.iUnionIDApi = this.wxmpSession.getMapper(IUnionIDApi.class);
	}

}
