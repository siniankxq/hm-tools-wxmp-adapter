package top.hmtools.wxmp.user.model;

import top.hmtools.wxmp.user.enums.LangType;

public class UserInfoParam {

	private String openid;
	
	private LangType lang;

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public LangType getLang() {
		return lang;
	}

	public void setLang(LangType lang) {
		this.lang = lang;
	}
	
	
}
