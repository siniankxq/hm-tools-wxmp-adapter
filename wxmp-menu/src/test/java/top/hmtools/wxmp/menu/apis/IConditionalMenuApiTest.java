package top.hmtools.wxmp.menu.apis;

import java.util.ArrayList;

import org.junit.Test;

import com.github.jsonzou.jmockdata.JMockData;

import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.menu.BaseTest;
import top.hmtools.wxmp.menu.models.conditional.ConditionalBean;
import top.hmtools.wxmp.menu.models.conditional.ConditionalMenuBean;
import top.hmtools.wxmp.menu.models.conditional.MatchruleBean;
import top.hmtools.wxmp.menu.models.conditional.TryMatchParamBean;
import top.hmtools.wxmp.menu.models.simple.Button;
import top.hmtools.wxmp.menu.models.simple.MenuWapperBean;

/**
 * 个性化菜单接口 测试
 * @author HyboWork
 *
 */
public class IConditionalMenuApiTest extends BaseTest{
	
	private IConditionalMenuApi conditionalMenuApi ;
	

	/**
	 * 创建个性化菜单
	 */
	@Test
	public void testaddConditional() {
		ConditionalMenuBean conditionalMenuBean = JMockData.mock(ConditionalMenuBean.class);
		
		String baseUrl = "hm.hn.cn";
		
		//底部第一个主按钮
		Button bbGuanwang = new Button();
		bbGuanwang.setName("官网").setType("view").setUrl("http://m.hybo.net/main/index/index.html");
		
		Button bbDongTai = new Button();
		bbDongTai.setName("动态").setType("view").setUrl("http://"+baseUrl+"/main/news/index.html");
		
		Button bbVideo = new Button();
		bbVideo.setName("视频").setType("view").setUrl("http://"+baseUrl+"/main/news/video.html");
		
		Button buttonBeanIndex = new Button();
		buttonBeanIndex.setName("哈哈");
		buttonBeanIndex.addSubButton(bbGuanwang,bbDongTai);
		
		conditionalMenuBean.setButton(new ArrayList<Button>());
		conditionalMenuBean.addButton(buttonBeanIndex);
		
		MatchruleBean matchrule = new MatchruleBean();
		matchrule.setCity("广州");
		matchrule.setClient_platform_type("2");
		matchrule.setCountry("中国");
		matchrule.setLanguage("zh_CN");
		matchrule.setProvince("广东");
		matchrule.setSex("1");
		matchrule.setTag_id("2");
		conditionalMenuBean.setMatchrule(matchrule);
		
		ConditionalBean conditionalBean = conditionalMenuApi.addConditional(conditionalMenuBean);
		this.printFormatedJson("创建个性化菜单", conditionalBean);
	}
	
	/**
	 * 测试删除个性化菜单
	 */
	@Test
	public void testdelConditional(){
		ConditionalBean conditionalBean = new ConditionalBean();
		conditionalBean.setMenuid("429779956");
		ErrcodeBean delConditional = this.conditionalMenuApi.delConditional(conditionalBean);
		this.printFormatedJson("测试删除个性化菜单",delConditional);
	}
	
	/**
	 * 测试个性化菜单匹配结果
	 */
	@Test
	public void testtryMatch(){
		TryMatchParamBean tryMatchParamBean = new TryMatchParamBean();
		tryMatchParamBean.setUser_id("o2ddm028EcvP1GZGBZG_chnnpc1Y");
		MenuWapperBean tryMatch = this.conditionalMenuApi.tryMatch(tryMatchParamBean);
		this.printFormatedJson("测试个性化菜单匹配结果", tryMatch);
	}
	
	//（可不用编写测试）测试 查询个性化菜单：使用普通自定义菜单查询接口可以获取默认菜单和全部个性化菜单信息，请见自定义菜单查询接口的说明。

	//（可不用编写测试） 测试 删除所有菜单： 使用普通自定义菜单删除接口可以删除所有自定义菜单（包括默认菜单和全部个性化菜单），请见自定义菜单删除接口的说明。


	@Override
	public void initSub() {
		this.conditionalMenuApi = this.wxmpSession.getMapper(IConditionalMenuApi.class);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
