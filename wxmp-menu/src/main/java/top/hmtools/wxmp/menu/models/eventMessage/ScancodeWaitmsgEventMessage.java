package top.hmtools.wxmp.menu.models.eventMessage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseEventMessage;
import top.hmtools.wxmp.core.model.message.enums.Event;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * scancode_waitmsg：扫码推事件且弹出“消息接收中”提示框的事件推送
 * <br>
 * <p>
 * <xml>
	<ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName>
	<FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName>
	<CreateTime>1408090606</CreateTime>
	<MsgType><![CDATA[event]]></MsgType>
	<Event><![CDATA[scancode_waitmsg]]></Event>
	<EventKey><![CDATA[6]]></EventKey>
	<ScanCodeInfo>
		<ScanType><![CDATA[qrcode]]></ScanType>
		<ScanResult><![CDATA[2]]></ScanResult>
	</ScanCodeInfo>
</xml>
 * </p>
 * @author Hybomyth
 *
 */
@WxmpMessage(msgType=MsgType.event,event=Event.scancode_waitmsg)
public class ScancodeWaitmsgEventMessage extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 338783581751355170L;

	/**
	 * 扫描信息
	 */
	@XStreamAlias("ScanCodeInfo")
	private ScanCodeInfo scanCodeInfo;

	/**
	 * 扫描信息
	 * @return
	 */
	public ScanCodeInfo getScanCodeInfo() {
		return scanCodeInfo;
	}

	/**
	 * 扫描信息
	 * @param scanCodeInfo
	 */
	public void setScanCodeInfo(ScanCodeInfo scanCodeInfo) {
		this.scanCodeInfo = scanCodeInfo;
	}

	@Override
	public void configXStream(XStream xStream) {
		
	}

	@Override
	public String toString() {
		return "ScancodeWaitmsgEventMessage [scanCodeInfo=" + scanCodeInfo + ", event=" + event + ", eventKey="
				+ eventKey + ", toUserName=" + toUserName + ", fromUserName=" + fromUserName + ", createTime="
				+ createTime + ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}

	
}
