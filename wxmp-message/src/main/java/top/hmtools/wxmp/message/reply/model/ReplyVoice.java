package top.hmtools.wxmp.message.reply.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复语音消息 中 的语音素材信息
 * @author hybo
 *
 */
public class ReplyVoice {

	/**
	 * 通过素材管理中的接口上传多媒体文件，得到的id
	 */
	@XStreamAlias("MediaId")
	private String mediaId;

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	@Override
	public String toString() {
		return "ReplyVoice [MediaId=" + mediaId + "]";
	}
	
	
}
