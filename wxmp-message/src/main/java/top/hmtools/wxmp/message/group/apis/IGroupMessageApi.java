package top.hmtools.wxmp.message.group.apis;

import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.core.enums.HttpParamType;
import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.message.group.model.DeleteParam;
import top.hmtools.wxmp.message.group.model.GroupMessageSendStatusParam;
import top.hmtools.wxmp.message.group.model.GroupMessageSendStatusResult;
import top.hmtools.wxmp.message.group.model.SpeedParam;
import top.hmtools.wxmp.message.group.model.SpeedResult;
import top.hmtools.wxmp.message.group.model.UploadImageParam;
import top.hmtools.wxmp.message.group.model.UploadImageResult;
import top.hmtools.wxmp.message.group.model.UploadNewsParam;
import top.hmtools.wxmp.message.group.model.UploadNewsResult;
import top.hmtools.wxmp.message.group.model.openIdGroupSend.BaseOpenIdGroupSendParam;
import top.hmtools.wxmp.message.group.model.openIdGroupSend.OpenIdGroupSendResult;
import top.hmtools.wxmp.message.group.model.preview.BasePreviewParam;
import top.hmtools.wxmp.message.group.model.preview.PreviewResult;
import top.hmtools.wxmp.message.group.model.tagGroupSend.BaseTagGroupSendParam;
import top.hmtools.wxmp.message.group.model.tagGroupSend.TagGroupSendResult;

@WxmpMapper
public interface IGroupMessageApi {

	/**
	 * 上传图文消息内的图片获取URL【订阅号与服务号认证后均可用】
	 * @param uploadImageParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri = "/cgi-bin/media/uploadimg",httpParamType=HttpParamType.FORM_DATA)
	public UploadImageResult uploadImage(UploadImageParam uploadImageParam);
	
	/**
	 * 上传图文消息素材【订阅号与服务号认证后均可用】
	 * @param uploadNewsParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/media/uploadnews")
	public UploadNewsResult uploadNews(UploadNewsParam uploadNewsParam);
	
	/**
	 * 根据标签进行群发【订阅号与服务号认证后均可用】
	 * @param tagGroupSendParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/message/mass/sendall")
	public TagGroupSendResult tagGroupMessageSend(BaseTagGroupSendParam tagGroupSendParam);
	
	/**
	 * 根据OpenID列表群发【订阅号不可用，服务号认证后可用】
	 * @param openIdGroupSendParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/message/mass/send")
	public OpenIdGroupSendResult openIdGroupMessageSend(BaseOpenIdGroupSendParam openIdGroupSendParam);
	
	/**
	 * 删除群发【订阅号与服务号认证后均可用】
	 * @param deleteParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/message/mass/delete")
	public ErrcodeBean deleteGroupMessageSended(DeleteParam deleteParam);
	
	/**
	 * 预览接口【订阅号与服务号认证后均可用】
	 * @param basePreviewParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/message/mass/preview")
	public PreviewResult previewGroupMessageSend(BasePreviewParam basePreviewParam);
	
	/**
	 * 查询群发消息发送状态【订阅号与服务号认证后均可用】
	 * @param groupMessageSendStatusParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/message/mass/get")
	public GroupMessageSendStatusResult getGroupMessageSendStatus(GroupMessageSendStatusParam groupMessageSendStatusParam	);
	
	/**
	 * 获取群发速度
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.GET,uri="/cgi-bin/message/mass/speed/get")
	public SpeedResult getGroupMessageSendSpeed();
	
	/**
	 * 设置群发速度
	 * @param speedParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/message/mass/speed/set")
	public ErrcodeBean setGroupMessageSendSpeed(SpeedParam speedParam);
	
	
	
	
	
}
