package top.hmtools.wxmp.message.group.model.tagGroupSend;

/**
 * Auto-generated: 2019-08-26 10:27:27
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class TextTagGroupSendParam extends BaseTagGroupSendParam {

	private Text text;

	public void setText(Text text) {
		this.text = text;
	}

	public Text getText() {
		return text;
	}

	@Override
	public String toString() {
		return "TextTagGroupSendParam [text=" + text + ", filter=" + filter + ", msgtype=" + msgtype + "]";
	}

}