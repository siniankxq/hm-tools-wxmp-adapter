package top.hmtools.wxmp.message.group.model.openIdGroupSend;

public class MpVideoOpenIdGroupSendParam extends BaseOpenIdGroupSendParam {

	private MpVideo mpvideo;

	public MpVideo getMpvideo() {
		return mpvideo;
	}

	public void setMpvideo(MpVideo mpvideo) {
		this.mpvideo = mpvideo;
	}

	@Override
	public String toString() {
		return "MpVideoOpenIdGroupSendParam [mpvideo=" + mpvideo + ", msgtype=" + msgtype + ", touser=" + touser + "]";
	}
}
