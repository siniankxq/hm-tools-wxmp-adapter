package top.hmtools.wxmp.message.template.apis;

import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.message.template.model.DelTemplateParam;
import top.hmtools.wxmp.message.template.model.GetIndustryResult;
import top.hmtools.wxmp.message.template.model.SendTemplateMessageParam;
import top.hmtools.wxmp.message.template.model.SendTemplateMessageResult;
import top.hmtools.wxmp.message.template.model.SetIndustryParam;
import top.hmtools.wxmp.message.template.model.TemplateIdParam;
import top.hmtools.wxmp.message.template.model.TemplateIdResult;
import top.hmtools.wxmp.message.template.model.TemplateListResult;

@WxmpMapper
public interface ITemplateApi {

	/**
	 * 设置所属行业
	 * @param setIndustryParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/template/api_set_industry")
	public ErrcodeBean setIndustry(SetIndustryParam setIndustryParam);
	
	/**
	 * 获取设置的行业信息
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.GET,uri="/cgi-bin/template/get_industry")
	public GetIndustryResult getIndustry();
	
	/**
	 * 获得模板ID
	 * @param templateIdParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/template/api_add_template")
	public TemplateIdResult getTemplateId(TemplateIdParam templateIdParam);
	
	/**
	 * 获取模板列表
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.GET,uri="/cgi-bin/template/get_all_private_template")
	public TemplateListResult getAllPrivateTemplate();
	
	/**
	 * 删除模板
	 * @param delTemplateParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/template/del_private_template")
	public ErrcodeBean delPrivateTemplate(DelTemplateParam delTemplateParam);
	
	/**
	 * 发送模板消息
	 * @param sendTemplateMessageParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/message/template/send")
	public SendTemplateMessageResult sendTemplateMessage(SendTemplateMessageParam sendTemplateMessageParam);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
