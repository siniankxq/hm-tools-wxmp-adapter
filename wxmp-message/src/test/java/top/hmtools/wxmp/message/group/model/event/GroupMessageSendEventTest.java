package top.hmtools.wxmp.message.group.model.event;

import org.junit.Test;

import top.hmtools.wxmp.core.DefaultWxmpMessageHandle;

public class GroupMessageSendEventTest {

	@Test
	public void testGroupMessageSendEvent() {
		// 实例化消息处理handle
		DefaultWxmpMessageHandle defaultWxmpMessageHandle = new DefaultWxmpMessageHandle();

		// 实例化实际处理指定消息的controller，并加入handle映射
		GroupMessageSendEventController menuMessageTestController = new GroupMessageSendEventController();
		defaultWxmpMessageHandle.addMessageMetaInfo(menuMessageTestController);

		//
		String xml = "<xml>   <ToUserName><![CDATA[gh_4d00ed8d6399]]></ToUserName>    <FromUserName><![CDATA[oV5CrjpxgaGXNHIQigzNlgLTnwic]]></FromUserName>    <CreateTime>1481013459</CreateTime>    <MsgType><![CDATA[event]]></MsgType>    <Event><![CDATA[MASSSENDJOBFINISH]]></Event>    <MsgID>1000001625</MsgID>    <Status><![CDATA[err(30003)]]></Status>    <TotalCount>0</TotalCount>    <FilterCount>0</FilterCount>    <SentCount>0</SentCount>    <ErrorCount>0</ErrorCount>    <CopyrightCheckResult>     <Count>2</Count>      <ResultList>       <item>         <ArticleIdx>1</ArticleIdx>          <UserDeclareState>0</UserDeclareState>          <AuditState>2</AuditState>          <OriginalArticleUrl><![CDATA[Url_1]]></OriginalArticleUrl>          <OriginalArticleType>1</OriginalArticleType>          <CanReprint>1</CanReprint>          <NeedReplaceContent>1</NeedReplaceContent>          <NeedShowReprintSource>1</NeedShowReprintSource>       </item>        <item>         <ArticleIdx>2</ArticleIdx>          <UserDeclareState>0</UserDeclareState>          <AuditState>2</AuditState>          <OriginalArticleUrl><![CDATA[Url_2]]></OriginalArticleUrl>          <OriginalArticleType>1</OriginalArticleType>          <CanReprint>1</CanReprint>          <NeedReplaceContent>1</NeedReplaceContent>          <NeedShowReprintSource>1</NeedShowReprintSource>       </item>     </ResultList>      <CheckState>2</CheckState>   </CopyrightCheckResult>   <ArticleUrlResult>     <Count>1</Count>     <ResultList>       <item>         <ArticleIdx>1</ArticleIdx>         <ArticleUrl><![CDATA[Url]]></ArticleUrl>       </item>     </ResultList>  </ArticleUrlResult></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
	}

}
